const express = require("express")
const app = express();
const {ApolloServer,gql}=require('apollo-server-express')
const mongoose=require("mongoose")
const {schema}=require("./type_def/schema")
const cors=require("cors")
const sample_data=require("./sampledata")

const {scrap}=require("./pdfscrap/scrap")


const server=new ApolloServer({schema})
server.applyMiddleware({app})

mongoose.connect("mongodb://localhost:27017/PC")
mongoose.connection.once("open",()=>{
    console.log("DB connection made")
})
scrap();

//sample_data.userdata_sample()
//sample_data.companydata_sample()
//sample_data.Notification_sample();
sample_data.Companylist_sample();
app.listen(4000, () => {
    console.log("listening to 4000")
})