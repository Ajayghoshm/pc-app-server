const mongoose=require("mongoose")
const Schema=mongoose.Schema

const Company=new Schema({
    Name:String,
    Job:String,
    Branch:[String],
    Backlogs:Number,
    History:Number,
    Semester:Number,
    Slot:String,
    Location:String,
    CloseTime:Date,
    External:[String],
    Date:Date,
    Logo:String,
    List_id:mongoose.Types.ObjectId,
    status:Number
})

module.exports=mongoose.model("Company",Company)