const moongoose=require("mongoose")
const Schema=moongoose.Schema

const companylist_schema=new Schema({
    reg_list:[moongoose.Schema.Types.ObjectId],
    result_list:[moongoose.Schema.Types.ObjectId],
    elig_list:[moongoose.Schema.Types.ObjectId],
    abs_list:[moongoose.Schema.Types.ObjectId],
    reg_full:[moongoose.Schema.Types.ObjectId],
    short_list:[moongoose.Schema.Types.ObjectId],
    name:String,
    company_id:moongoose.Schema.Types.ObjectId
})

module.exports=moongoose.model("Company_list",companylist_schema) 