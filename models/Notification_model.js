const mongoose=require('mongoose')
const Schema=mongoose.Schema

const Notification=new Schema(
        {
            count:Number,
            content:String,
            completed:Boolean
        }
)

module.exports=mongoose.model("Notification",Notification)