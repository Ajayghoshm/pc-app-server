const mongoose = require("mongoose")
const schema = mongoose.Schema

const User = new schema({
    name: {
        f_name: String,
        l_name: String
    },
    status:Number,
    address_1: {
        h_name: String,
        street: String,
        district: String,
        state: String,
        pincode: Number,
    },
    address_2: {
        h_name: String,
        street: String,
        district: String,
        state: String,
        pincode: Number,
    },
    phone: {
        no_1: Number,
        no_2: Number
    },
    email: {
        email_1: String,
        email_2: String
    },
    dob: Date,
    ktu_no: String,
    admin_no: String,
    roll_no: String,
    tcgpa: String,
    ids: {
       cgpa: mongoose.Types.ObjectId,
        academic: mongoose.Types.ObjectId
    },
    company:[{
        name:String,
        pay:String,
        slot:String
    }],
    photo_id: String,
})

module.exports = mongoose.model("User", User)