const usermodel = require("./models/User_model")
const companymodel=require("./models/Company_model")
const notificationmodel=require("./models/Notification_model")
const companylistmodel=require("./models/Companylist_model")
const mongoose=require("mongoose")

userdata_sample=()=>{
 let user = new usermodel({
name: {
    f_name: "utthara",
    l_name: "ghosh"
},
address_1: {
    h_name: "mangad",
    street: "Niramarthur",
    district: "Malappuram",
    state: "Kerala",
    pincode: 676109,
},
address_2: {
    h_name: "mangad",
    street: "Niramarthur",
    district: "Malappuram",
    state: "Kerala",
    pincode: 676109,
},
phone: {
    no_1: 9995355668,
    no_2: 9495452535
},
email: {
    email_1: "ajayghosh668@gmail.com",
    email_2: "ajayfreakz@gmail.com"
},
dob: new Date(2018, 11, 24, 10, 33, 30, 0),
ktu_no: "LMDL15CS121",
admin_no: "7468/16",
roll_no: "62",
tcgpa: "6.8",
ids: {
   cgpa: mongoose.Types.ObjectId("5bebd3cccdd09523ecf8a0bc"),
    academic: mongoose.Types.ObjectId("5bebd3cccdd09523ecf8a0bc")
},
company:[{
    name:"Tata",
    pay:"4lpa",
    slot:"A"
}],
photo_id: "sadasd",
})
return user.save()
}

companydata_sample=()=>{
    let company=new companymodel({
        'Name':"TCS",
        'Job':"Network engineer",
        'Branch':["CS","EEE","ECE"],
        'Backlogs':2,
        'History':1,
        'Semester':4,
        'Slot':"A",
        'Location':"Bangolore",
        'CloseTime':new Date(2018, 11, 24, 10, 33, 30, 0) ,
        'External':["Fisat"],
        'Date':new Date(2018, 11, 24, 10, 33, 30, 0),
        'Logo':"photoid",
        'List_id':mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        'status':1
    })
    return company.save()
}

Notification_sample=()=>{new notificationmodel({
    'count': 1,
    'content': 'Regisitrations Open',
    'completed': false
  }
  ).save()}


Companylist_sample=()=>{new companylistmodel({
'reg_list':[mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")],
'elig_list':[mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")],
'abs_list':[mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")],
'short_list':[mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")],
'reg_full':[mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")],
'result_list':[mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2"),
        mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")],
'name':"KPMG",
'company_id':mongoose.Types.ObjectId("5beac691c0833d3264e7d6e2")
}).save()
}
module.exports={userdata_sample,companydata_sample,Notification_sample,Companylist_sample}