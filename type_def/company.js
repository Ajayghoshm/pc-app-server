/*const Company = new GraphQLObjectType({
    name: "Company",
    fields: () => ({
        status: { type: GraphQLInt },
        Name: { type: GraphQLString },
        Job: {type:GraphQLString},
        Branch:{type:GraphQLList(GraphQLString)},
        Backlogs:{type:GraphQLInt},
        History: {type:GraphQLInt},
        Semester: {type:GraphQLInt},
        Slot: {type:GraphQLString},
        Location: {type:GraphQLString},
        CloseTime: {type:GraphQLString},
        External:{type:GraphQLList(GraphQLString)},
        Date: {type:GraphQLString},
        Logo: {type:GraphQLString},
        List_id: {type:GraphQLID} 
    })
})*/
const {gql} =require("apollo-server-express")
const company_model =require("../models/Company_model")
const Companylist_model=require("../models/Companylist_model")


const Company_type=gql`
type Company{
        id:ID,
        status:Int,
        Name:String,
        Job: String,
        Branch:[String],
        Backlogs:Int,
        History: Int,
        Semester: Int,
        Slot: String,
        Location: String,
        CloseTime: String,
        External:[String],
        Date: String,
        Logo: String,
        List_id: ID 
}
`
const company_resolver={
Query:{
    company_eligiblity_check:async(_,args)=>{
        try{
            let company_response=await company_model.findById(args.company_id)
            console.log("companyresponse",company_response)
            let companylist_id=company_response.List_id
            console.log("companylistid",companylist_id)
            let companylist_response=await Companylist_model.findById(companylist_id)
            console.log(companylist_response.elig_list)
            let elig_list=companylist_response.elig_list
            let value=false
            elig_list.find((element)=>{
                if (element==args.user_id){
                    value=true
                }
            })
            console.log(value)
            return value
        }catch(e){
            console.log("error",e)
            return false
        }
    }
}
    ,
    Mutation:{
        companyapply:async(_,args)=>{
            try{
            let company_response=await company_model.findById(args.company_id)
            console.log(company_response)
            let companylist_id=company_response.List_id
            console.log("companylistid",companylist_id)
            let companylist_response=await Companylist_model.findById(companylist_id)
            console.log(companylist_response)
            companylist_response.reg_list.push(args.user_id)
            await companylist_response.save()
            console.log(response)
            return response
        }catch (e){
            console.log(e)
        }
    }
    }

}

module.exports={Company_type,company_resolver}