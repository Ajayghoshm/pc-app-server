const  {gql} =require('apollo-server-express')

const company_list=gql`type company_list{
    reg_list:[ID],
    result_list:[ID],
    elig_list:[ID],
    abs_list:[ID],
    short_list:[ID],
    reg_full:[ID],
    company_id:ID,
    name:String
}   
`
module.exports={company_list}