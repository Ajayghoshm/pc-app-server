
const graphql = require("graphql")
const { GraphQLInputObjectType, GraphQLSchema, GraphQLBoolean, GraphQLID, GraphQLList, GraphQLInt, GraphQLNonNull, GraphQLFloat, GraphQLString, GraphQLObjectType } = graphql
const company_model = require("../models/Company_model")
const Companylist_model=require("../models/Companylist_model")
//const {companylist_Schema}=require("../schema/companylist_Schema")


const Company = new GraphQLObjectType({
    name: "Company",
    fields: () => ({
        status: { type: GraphQLInt },
        Name: { type: GraphQLString },
        Job: { type: GraphQLString },
        Branch: { type: GraphQLList(GraphQLString) },
        Backlogs: { type: GraphQLInt },
        History: { type: GraphQLInt },
        Semester: { type: GraphQLInt },
        Slot: { type: GraphQLString },
        Location: { type: GraphQLString },
        CloseTime: { type: GraphQLString },
        External: { type: GraphQLList(GraphQLString) },
        Date: { type: GraphQLString },
        Logo: { type: GraphQLString },
        List_id: { type: GraphQLID }
    })
})

const Notfication_graphql = new GraphQLObjectType({
    name: "Notification_graphql",
    fields: () => ({
        count: { type: GraphQLInt },
        content: { type: GraphQLString },
        completed: { type: GraphQLBoolean }
    })
})

const user_company = new GraphQLObjectType({
    name: "user_company_list",
    fields: () => ({
        name: { type: GraphQLString },
        slot: { type: GraphQLString },
        pay: { type: GraphQLString }
    })
})
const Usertype = new GraphQLObjectType({
    name: "User",
    description: "Contains user details,upcoming Drives",
    fields: () => ({
        id: { type: GraphQLID },
        name: {
            type: new GraphQLObjectType({
                name: "name",
                fields: () => ({
                    f_name: { type: GraphQLString },
                    l_name: { type: GraphQLString },
                })
            })
        },
        phone: {
            type: new GraphQLObjectType({
                name: "phone",
                fields: () => ({
                    no_1: { type: GraphQLInt },
                    no_2: { type: GraphQLInt }
                })
            })
        },
        email: {
            type: new GraphQLObjectType({
                name: "email",
                fields: () => ({
                    email_1: { type: GraphQLString },
                    email_2: { type: GraphQLString }
                })
            })
        },
        address_1: {
            type: new GraphQLObjectType({
                name: "address_1",
                fields: () => ({
                    h_name: { type: GraphQLString },
                    street: { type: GraphQLString },
                    district: { type: GraphQLString },
                    state: { type: GraphQLString },
                    pincode: { type: GraphQLInt }
                })
            })
        },
        address_2: {
            type: new GraphQLObjectType({
                name: "address_2",
                fields: () => ({
                    h_name: { type: GraphQLString },
                    street: { type: GraphQLString },
                    district: { type: GraphQLString },
                    state: { type: GraphQLString },
                    pincode: { type: GraphQLInt }
                })
            })
        },
        dob: { type: GraphQLString },
        ktu_no: { type: GraphQLString },
        admin_no: { type: GraphQLString },
        roll_no: { type: GraphQLInt },
        tcgpa: { type: GraphQLFloat },
        ids: {
            type: new GraphQLObjectType({
                name: "ids",
                fields: () => ({
                    cgpa: { type: GraphQLString },
                    academic: { type: GraphQLString }
                })
            })
        },
        company: { type: new GraphQLList(user_company) },
        photo_id: { type: GraphQLString },
        upcoming_Drives: {
            type: new GraphQLList(Company),
            description: "All drives that are open now",
            resolve(parent, args) {
                console.log("parent", parent.id)
                let found = new Promise((resolve, reject) => {
                    company_model.find({ "status": 1 }, (err, result) => {
                        if (err) {
                            reject("none", err)
                        }
                        else {

                            resolve(result)
                        }
                    })
                })
                return found
            }
        },
        Registered_Drives: {
            type: new GraphQLList(Company),
            description: "All drives that are registered",
            resolve(parent, args) {
                console.log("parent", parent.id)
                let found = new Promise((resolve, reject) => {
                    Companylist_model.find({ reg_list: parent.id }, (err, result) => {
                        if (err) {
                            reject("none", err)
                        }
                        else {
                            idarray=[]
                            result.forEach(element => {
                                idarray.push(element.company_id)
                            });
                            console.log("array of object_ids",idarray)
                            company_model.find({"_id":{$in:idarray}},(err,res)=>{
                                if(err){
                                    reject(err)
                                }
                                else{
                                    console.log("final list of companies",res)
                                    resolve(res)
                                }
                            })
                            // var resultarray=[]
                            // result.forEach(element => {
                            //     company_model.find({"_id":element.company_id}, (err, res) => {
                            //         if (err) {
                            //             reject("none", err)
                            //         }
                            //         else {
                            //             console.log("final list", res)
                            //             resultarray.push(res)
                            //             resolve(resultarray)
                            //         }
                                    
                            //     })
                            // })
                         }
                    })
                })
                return found
            }
        }
    })
})
module.exports = { Usertype, Notfication_graphql }