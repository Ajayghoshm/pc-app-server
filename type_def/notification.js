/*const Notfication_graphql=new GraphQLObjectType({
    name:"Notification_graphql",
    fields:()=>({
        count:{type:GraphQLInt},
        content:{type:GraphQLString},
        completed:{type:GraphQLBoolean}
    })
})*/

const {gql} =require('apollo-server-express')
const notification_model = require("../models/Notification_model")

const notification_type=gql`
type notification{
    count:Int
    content:String
    completed:Boolean
    id:ID
}   
`
const noti_resolver={
    Query:{
        notification_query: async()=>{
            try{
            let response=await notification_model.find({})
            console.log("notifiction response",response)
            return response
            }
            catch(e){
                console.log("moongo_notification_error",e)
            }
    }
}
}

module.exports={notification_type,noti_resolver}