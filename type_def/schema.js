//moongoose user model
const usermodel = require("../models/User_model")
//gql from apollo server
const { gql,makeExecutableSchema } = require('apollo-server-express')
//user_schema graphql
const user_schema=require('./user')
const noti_schema=require("./notification")
const company_model = require("../type_def/company")
const companylist_model=require("../type_def/company_list_type")
const notification_model = require("../models/Notification_model")
const _ = require("lodash")
const data = require("./data")

const { Usertype, company, Notfication_graphql } = data

const schema=makeExecutableSchema({
    typeDefs:[user_schema.user_type,noti_schema.notification_type,company_model.Company_type,companylist_model.company_list],
    resolvers:[user_schema.resolver,noti_schema.noti_resolver,company_model.company_resolver]
})

module.exports={schema}
/*const rootQuery = new GraphQLObjectType(
    {
        name: "user_Query",
        fields: {
            user: {
                type: Usertype,
                args: { id: { type: GraphQLID } },
                resolve(parent, args) {
                    console.log(args.id)
                    // var userv=_.find(user,{id: args.id})
                    // console.log(userv)
                    // return userv
                    let found = new Promise((resolve, reject) => {
                        usermodel.findById(args.id, (err, user) => {
                            if (err) {
                                reject("no match found", err)
                            }
                            else {
                                resolve(user)
                                console.log("found match", user)
                            }
                        })
                    })
                    return found
                }

            },
            notification: {
                type: new GraphQLList(Notfication_graphql),
                resolve(parent, args) {
                    let found = new Promise((resolve, reject) => {
                        notification_model.find({}, (err, res) => {
                            if (err) {
                                reject("no match found", err)
                            }
                            else {
                                resolve(res)
                                console.log("found match", res)
                            }
                        })
                    })
                    return found
                }
            }
        }
    })


const mutation = new GraphQLObjectType({
    name: "user_Mutation",
    fields: {
        adduser: {
            type: Usertype,
            args: { data: { type: userinput } },
            resolve(parent, args) {
                let user = new usermodel({
                    'name.f_name': args.data.f_name,
                    'name.l_name': args.data.l_name,
                    'phone.no_1': args.data.no_1,
                    'company': args.data.company
                    // fname:args.fname,
                    // lname:args.lname
                })
                return user.save()
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: rootQuery,
    mutation: mutation
})*/