const usermodel = require("../models/User_model")
const { gql } = require('apollo-server-express')

const Companylist_model=require("../models/Companylist_model")
const company_model =require("../models/Company_model")




const user_type = gql`
type user{
    id:ID!
    name:name
    address_1:address
    address_2:address
    phone:phone
    email:email
    dob: String
    ktu_no: String
    admin_no:String
    roll_no: Int 
    company: [user_company]
    ids:ids
    tcgpa:String
    photoid: String
    upcoming_Drives(id:ID):[Company]
    Register_Drives:[Company]
    status:Int
    }
    type name{
    f_name:String
    l_name:String
    }
    type address{
        h_name: String,
        street: String,
        district: String,
        state: String,
        pincode: Int,
    }
    type phone{
        no_1: Float
    no_2: Float
    }
    type email{
        email_1: String
    email_2: String
    }
    type user_company{
        id:ID
        name:String
        pay:String
        slot:String
    }
    type ids{
        cgpa:ID
        academic:ID
    }
    type bool{
        value:Boolean
    }

type Query{
    UserQuery(id:String!):user
    notification_query:[notification]
    company_eligiblity_check(company_id:String!,user_id:String!):Boolean
}

type Mutation{
    permission(id:String!):user
    companyapply(user_id:String!,company_id:String!):Boolean
}
`
const resolver = {
    Query: {
        UserQuery: async (_, args) => {
            try {
                let response = await usermodel.findById(args.id)
                if(response.status==3){
                    response.status=true
                }
                else{
                    response.status=false
                }
                console.log(response)
                return response
            } catch (e) {
                console.log("Mongoquery_error", e)
            }
        }
    },
    Mutation:{
        permission:async(_,args)=>{
            try{
            let response=await usermodel.findById(args.id)
            console.log(response)
            response.status=2
            await response.save()
            console.log(response)
            return response
        }catch (e){
            console.log(e)
        }
    }
    },
    user:{
    upcoming_Drives:async(parent)=>{
        console.log(parent)
        let response=await company_model.find({ "status": 1 })
        return response
    },
        Register_Drives:async(parent)=>{
            console.log(parent)
            let response=await Companylist_model.find({reg_list:parent.id})
            console.log(response)
            let company_id=[];
            response.forEach(company=>{
                company_id.push(company.company_id)
            })
            console.log(company_id)
            
            let response2=await company_model.find().where('_id').in(company_id)
            console.log(response2)
            return response2
}
}
}

module.exports = { user_type, resolver }